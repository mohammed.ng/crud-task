import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:task/model/user_model.dart';
import 'dart:io' as Io;
import 'dart:convert' as convert;
import 'dart:io';

import 'package:task/sqlite/db_helper.dart';

class MainController extends GetxController {
  late TextEditingController name, dob, nationality, salary;

  var gender = ("Male").obs;
  late File image;

  @override
  void onInit() {
    ///calling createuserdetailstable
    createUserDetailsTable();

    ///get User list
    getUserList();

    name = TextEditingController();
    dob = TextEditingController();
    nationality = TextEditingController();
    salary = TextEditingController();

    super.onInit();
  }

  ///Image picker
  final picker = ImagePicker();
  dynamic fileImage64;

  ///Image from camera
  Future getImageFromCamera() async {
    XFile? pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 25);
    if (pickedFile != null) {
      final bytes = await Io.File(pickedFile.path).readAsBytes();
      fileImage64 = convert.base64Encode(bytes);
      image = File(pickedFile.path);
    } else {
      print('No image selected.');
    }
    update();
  }

  ///Image from gallery
  Future getImageFromGallery() async {
    XFile? pickedFile =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 25);
    if (pickedFile != null) {
      final bytes = await Io.File(pickedFile.path).readAsBytes();
      fileImage64 = convert.base64Encode(bytes);
      image = File(pickedFile.path);
    } else {
      print('No image selected.');
    }
    update();
  }

  ///Create User details table
  createUserDetailsTable() async {
    final db = await DBHelper.instance.database;
    await db.execute(
        "CREATE TABLE IF NOT EXISTS list_data( id INTEGER PRIMARY KEY NOT NULL, name TEXT NOT NULL,dob TEXT , nationality TEXT, salary TEXT, gender TEXT, image TEXT)");
  }

  ///Set filteredUserList
  List<UserModel> userList = [];
  List<UserModel> get filteredUserList => userList;
  set filteredUserList(List<UserModel> name) {
    // ignore: deprecated_member_use
    userList = name;
  }

  ///filter user list
  void filterUserList(string) {
    if (string == "") {
      getUserList();
    } else {
      // ignore: invalid_use_of_protected_member
      filteredUserList = userList
          .where((u) => (u.name.toLowerCase().contains(string.toLowerCase())))
          .toList();
      update();
    }
  }

  ///Get user list
  getUserList() async {
    userList.clear();
    update();
    final db = await DBHelper.instance.database;
    db.query("list_data").then((value) {
      value.forEach((element) {
        userList.add(UserModel.fromJson(element));
        update();
      });
    });
  }

  ///Add Data to User lsit table
  addListData(context) async {
    final db = await DBHelper.instance.database;
    dynamic addData = {
      "name": name.text,
      "dob": dob.text,
      "nationality": nationality.text,
      "salary": salary.text,
      "gender": gender.value,
      "image": fileImage64
    };
    db.insert("list_data", addData).then((value) {
      getUserList();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Added successfully')),
      );
      Get.back();
    });
  }

  ///Update user data
  updateData(context, int? id) async {
    final db = await DBHelper.instance.database;
    dynamic addData = {
      "name": name.text,
      "dob": dob.text,
      "nationality": nationality.text,
      "salary": salary.text,
      "gender": gender.value,
      "image": fileImage64
    };
    db.update("list_data", addData, where: "id = ?", whereArgs: [id]).then(
        (value) {
      getUserList();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Updated successfully')),
      );
      Get.back();
    });
  }
}
