import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task/controller/main_controller.dart';
import 'package:task/model/user_model.dart';

class Date extends StatelessWidget {
  final String title;
  final UserModel? user;

  Date({Key? key, required this.title, this.user}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  final MainController mainController = Get.find();

  @override
  Widget build(BuildContext context) {
    /// If title is update assign value
    if (title == "Update") {
      mainController.fileImage64 = user!.image;
      mainController.gender.value = user!.gender;
      mainController.salary.text = user!.salary;
      mainController.nationality.text = user!.nationality;
      mainController.name.text = user!.name;
      mainController.dob.text = user!.dob.toString();
    } else {
      mainController.fileImage64 = null;
      mainController.gender.value = "";
      mainController.salary.text = "";
      mainController.nationality.text = "";
      mainController.name.text = "";
      mainController.dob.text = "";
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("$title data"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            mainController.fileImage64 == null
                ? ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text('Pick Image'),
                  ))
                : title == "Add"
                    ? mainController.addListData(context)
                    : mainController.updateData(context, user!.id);
          }
        },
        child: Text(title),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GetBuilder<MainController>(
                    builder: (controller) => controller.fileImage64 == null
                        ? Container()
                        : GestureDetector(
                            onTap: () => showDialog(
                                  builder: (BuildContext context) => SizedBox(
                                      height: Get.height / 2,
                                      width: Get.width / 2,
                                      child: Image.memory(base64Decode(
                                          controller.fileImage64))),
                                  context: context,
                                ),
                            child: CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius: 64.0,
                                backgroundImage: MemoryImage(
                                  base64Decode(controller.fileImage64),
                                )))),
                ElevatedButton(
                    onPressed: () {
                      Get.bottomSheet(
                          Container(
                            padding:
                                const EdgeInsets.only(top: 18.0, bottom: 18.0),
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Text(
                                  "Choose photo from :",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 18),
                                ),
                                FloatingActionButton(
                                    elevation: 0,
                                    tooltip: "Camera",
                                    onPressed: () =>
                                        mainController.getImageFromCamera(),
                                    backgroundColor: Colors.amberAccent,
                                    child: const Icon(Icons.camera)),
                                FloatingActionButton(
                                    elevation: 0,
                                    focusColor: Colors.greenAccent,
                                    tooltip: "Gallery"
                                        "",
                                    onPressed: () =>
                                        mainController.getImageFromGallery(),
                                    backgroundColor: Colors.indigoAccent,
                                    child: const Icon(Icons.photo_album)),
                              ],
                            ),
                          ),
                          barrierColor: Colors.black12.withOpacity(0.5));
                    },
                    child: const Text("Select Image")),
                TextFormField(
                  controller: mainController.name,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter name';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    label: Text("Name"),
                  ),
                ),
                TextFormField(
                  controller: mainController.dob,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter dob';
                    }
                    return null;
                  },
                  readOnly: true,
                  decoration: const InputDecoration(
                    label: Text("D.O.B"),
                  ),
                  onTap: () {
                    showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1950),
                            lastDate: DateTime.now())
                        .then((value) {
                      mainController.dob.text = value.toString();
                    });
                  },
                ),
                TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter nationality';
                    }
                    return null;
                  },
                  controller: mainController.nationality,
                  decoration: const InputDecoration(
                    label: Text("Nationality"),
                  ),
                ),
                TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter salary';
                    }
                    return null;
                  },
                  controller: mainController.salary,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                    label: Text("Salary"),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                const Text(
                  "Gender",
                  style: TextStyle(fontSize: 18),
                ),
                Row(
                  children: [
                    Flexible(
                      child: Obx(() => ListTile(
                          title: const Text('Male'),
                          leading: Radio(
                            value: "Male",
                            groupValue: mainController.gender.value,
                            onChanged: (val) {
                              mainController.gender.value = val.toString();
                            },
                          ))),
                    ),
                    Flexible(
                      child: Obx(() => ListTile(
                          title: const Text('Female'),
                          leading: Radio(
                            value: "Female",
                            groupValue: mainController.gender.value,
                            onChanged: (val) {
                              mainController.gender.value = val.toString();
                            },
                          ))),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
