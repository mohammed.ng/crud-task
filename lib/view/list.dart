import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task/controller/main_controller.dart';
import 'package:task/sqlite/db_helper.dart';
import 'package:task/view/add_update_data.dart';

class ListData extends StatelessWidget {
  ListData({Key? key}) : super(key: key);

  ///Initializing Main Controller
  final MainController mainController = Get.put(MainController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("List of Users"),
        actions: [
          TextButton(
              onPressed: () => Get.to(() => Date(title: "Add"),
                  transition: Transition.rightToLeft),
              child: const Text(
                "Add User",
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextField(
              decoration: const InputDecoration(label: Text("Search by Name")),
              onChanged: (value) {
                mainController.filterUserList(value);
              },
            ),
          ),
          GetBuilder<MainController>(
              builder: (controller) => controller.filteredUserList.isEmpty
                  ? const Expanded(
                      child: Center(
                        child: Text("No users found"),
                      ),
                    )
                  : Expanded(
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) =>
                            ListTile(
                          leading: Image.memory(
                            base64Decode(
                                controller.filteredUserList[index].image),
                            height: 48,
                            width: 48,
                          ),
                          title: Text(controller.filteredUserList[index].name),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  "DOB: ${controller.filteredUserList[index].dob.toString().split(" ")[0]}"),
                              Text(
                                  "Nationality: ${controller.filteredUserList[index].nationality}"),
                              Text(
                                  "Salary: ₹${controller.filteredUserList[index].salary}"),
                              Text(
                                  "Gender: ${controller.filteredUserList[index].gender}"),
                            ],
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              IconButton(
                                icon: const Icon(Icons.edit),
                                onPressed: () => Get.to(
                                    Date(
                                        title: "Update",
                                        user:
                                            controller.filteredUserList[index]),
                                    transition: Transition.rightToLeft),
                              ),
                              IconButton(
                                icon: const Icon(Icons.delete),
                                onPressed: () async {
                                  final db = await DBHelper.instance.database;
                                  db.delete("list_data",
                                      where: "id = ?",
                                      whereArgs: [
                                        controller.filteredUserList[index].id
                                      ]).then((value) {
                                    controller.getUserList();
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                          content:
                                              Text('Deleted successfully')),
                                    );
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                        itemCount: controller.userList.length,
                      ),
                    )),
        ],
      ),
    );
  }
}
