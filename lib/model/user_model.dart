import 'dart:convert';

List<UserModel> userModelFromJson(String str) =>
    List<UserModel>.from(json.decode(str).map((x) => UserModel.fromJson(x)));

String userModelToJson(List<UserModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UserModel {
  UserModel({
    required this.id,
    required this.name,
    required this.dob,
    required this.nationality,
    required this.salary,
    required this.gender,
    required this.image,
  });

  int id;
  String name;
  DateTime dob;
  String nationality;
  String salary;
  String gender;
  String image;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"],
        name: json["name"],
        dob: DateTime.parse(json["dob"]),
        nationality: json["nationality"],
        salary: json["salary"],
        gender: json["gender"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "dob": dob.toIso8601String(),
        "nationality": nationality,
        "salary": salary,
        "gender": gender,
        "image": image,
      };
}
