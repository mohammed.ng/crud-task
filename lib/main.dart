import 'package:flutter/material.dart';
import 'package:task/sqlite/db_helper.dart';
import 'app.dart';

void main() {
  ///binding will be initialized before runApp
  WidgetsFlutterBinding.ensureInitialized();

  ///DataBase Helper
  DBHelper.instance.database;

  runApp(const MyApp());
}
