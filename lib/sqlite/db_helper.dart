import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper {
  static final DBHelper instance = DBHelper._init();
  static Database? _database;

  ///Database name
  final String databaseName = "task.db";

  DBHelper._init();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDB(databaseName);
    return _database!;
  }

  // ignore: missing_return
  Future<Database> _initDB(String filePath) async {
    ///Assign path
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    ///Open Database
    return await openDatabase(path, version: 1, onCreate: onCreate);
  }

  static void onCreate(Database db, int version) async {
    ///Create UserDetails Table
    await db.execute(
        'CREATE TABLE user_details (pid INTEGER PRIMARY KEY NOT NULL,name TEXT, password TEXT )');

    dynamic body = {"name": "hari", "password": "hari@112"};

    ///insert User data
    await db.insert("user_details", body);
  }

  ///Close Database
  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
